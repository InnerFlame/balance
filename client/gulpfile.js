'use strict';

//http://balance.client:8080/
//gulp

var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var connect = require('gulp-connect');
var livereload = require('gulp-livereload');

gulp.task('connect', function() {
    connect.server({root: 'public', livereload: true, host: 'http://balance.client/'});
});

gulp.task('html', function () {
    return gulp.src('public/index.html')
        .pipe(connect.reload());
});

gulp.task('css', function () {
    return gulp.src('src/**/*.css')
        .pipe(concatCss('app.css'))
        .pipe(autoprefixer())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('public/css/'))
        .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch('src/**/*.css', ['css'])
    gulp.watch('public/index.html', ['html'])
});

gulp.task('default', ['connect', 'html', 'css', 'watch']);