<?php

use yii\db\Migration;

class m160831_184508_init extends Migration
{
    const TBL_USERS = 'users';
    const TBL_BALANCES = 'balances';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TBL_USERS, [
            'id'         => $this->primaryKey(),
            'username'   => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name'  => $this->string()->notNull(),
            'email'      => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable(self::TBL_BALANCES, [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer(),
            'amount'     => $this->float()->notNull(),
            'sign'       => $this->smallInteger()->notNull(),
            'type'       => $this->smallInteger()->notNull(),
            'comment'    => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_' . self::TBL_BALANCES . 'user_id', self::TBL_BALANCES, 'user_id', self::TBL_USERS, 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_' . self::TBL_BALANCES . 'user_id', self::TBL_BALANCES);

        $this->dropTable(self::TBL_USERS);
        $this->dropTable(self::TBL_BALANCES);
    }
}
