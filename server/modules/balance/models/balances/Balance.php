<?php

namespace app\modules\balance\models\balances;

use app\modules\balance\models\users\User;
use Yii;

/**
 * This is the model class for table "balances".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property integer $sign
 * @property integer $type
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Balance extends \yii\db\ActiveRecord
{
//    public function extraFields()
//    {
//        return ['user'];
//    }
//http://balance.server/balance/balances?expand=user
//https://github.com/yiisoft/yii2/blob/master/docs/guide-ru/rest-resources.md

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balances';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sign', 'type', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'sign', 'type'], 'required'],
            [['amount'], 'number'],
            [['comment'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'user_id'    => Yii::t('app', 'User ID'),
            'amount'     => Yii::t('app', 'Amount'),
            'sign'       => Yii::t('app', 'Sign'),
            'type'       => Yii::t('app', 'Type'),
            'comment'    => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return BalanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BalanceQuery(get_called_class());
    }
}
