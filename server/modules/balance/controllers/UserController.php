<?php

namespace app\modules\balance\controllers;

use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'app\modules\balance\models\users\User';
}