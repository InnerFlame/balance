<?php

namespace app\modules\balance\controllers;

use app\modules\balance\models\balances\Balance;
use app\modules\balance\models\users\User;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class BalanceController extends ActiveController
{
    public $modelClass = 'app\modules\balance\models\balances\Balance';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['view'], $actions['create'], $actions['update'], $actions['delete']);
        return $actions;
    }

    public function actionIndex($user_id)
    {
        return Balance::find()->with('user')->andWhere(['user_id' => $user_id])->all();
    }

    public function actionView($user_id, $balance_id)
    {
        $balance = Balance::find()->with('user')->andWhere(['id' => $balance_id, 'user_id' => $user_id])->one();
        return (is_object($balance)) ? $balance : new NotFoundHttpException();
    }

    public function actionCreate($user_id)
    {
        $user = User::findOne($user_id);
        if(!is_object($user)){
            return new NotFoundHttpException();
        }

        $balance = new Balance();
        if($balance->load(\Yii::$app->request->post()) && $balance->save()){
            return $balance;
        }
    }

    public function actionUpdate($user_id, $balance_id)
    {
        $balance = Balance::find()->with('user')->andWhere(['id' => $balance_id, 'user_id' => $user_id])->one();

        if(!is_object($balance)){
            return new NotFoundHttpException();
        }

        if($balance->load(\Yii::$app->request->bodyParams, '') && $balance->save()){
            return $balance;
        }
    }

    public function actionDelete($user_id, $balance_id)
    {
        $balance = Balance::find()->with('user')->andWhere(['id' => $balance_id, 'user_id' => $user_id])->one();
        if(!is_object($balance)){
            return new NotFoundHttpException();
        }
    }
}